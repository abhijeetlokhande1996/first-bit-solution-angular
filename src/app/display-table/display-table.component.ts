import { Component, OnInit, Input } from '@angular/core';
import { UserModel } from '../models/user.model';

@Component({
  selector: 'app-display-table',
  templateUrl: './display-table.component.html',
  styleUrls: ['./display-table.component.css'],
})
export class DisplayTableComponent implements OnInit {
  // @Input('data') displayData: Array<{}>;

  @Input('myArray') numberArr: Array<number>;
  @Input('my_string') display_string: string;
  @Input('my_number') display_number: number;
  @Input('my_boolean') display_bool: boolean;

  @Input('objArray') objArray: Array<{}>;

  @Input('objArray2') objArray2;
  constructor() {}

  ngOnInit(): void {}

  displayObjArr() {
    console.table(this.objArray);
  }
}
