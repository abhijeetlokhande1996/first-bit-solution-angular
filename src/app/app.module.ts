import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgIfDemoComponent } from './ng-if-demo/ngIfDemo.component';
import { NgForDemoComponent } from './ng-for-demo/ng-for-demo.component';
import { FormsModule } from '@angular/forms';
import { MyUpperCasePipe } from './../pipes/my-upper-case.pipe';
import { RegFormsComponent } from './reg-forms/reg-forms.component';
import { FilterPipe } from './pipes/filter.pipe';
import { DisplayTableComponent } from './display-table/display-table.component';
@NgModule({
  declarations: [
    AppComponent,
    NgIfDemoComponent,
    NgForDemoComponent,
    MyUpperCasePipe,
    RegFormsComponent,
    FilterPipe,
    DisplayTableComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
