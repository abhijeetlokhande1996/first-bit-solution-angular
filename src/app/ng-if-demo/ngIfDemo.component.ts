import { Component } from '@angular/core';
@Component({
  selector: 'app-ngifdemo',
  templateUrl: './ngIfDemo.component.html',
})
export class NgIfDemoComponent {
  btnText: string = 'On';
  displayPara: boolean = true;
  toggle() {
    this.displayPara = !this.displayPara;
    this.btnText = this.displayPara ? 'On' : 'Off';
  }
}
