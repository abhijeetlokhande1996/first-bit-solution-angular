import { Pipe, PipeTransform } from '@angular/core';
import { UserModel } from '../models/user.model';

@Pipe({
  name: 'filterPipe',
  pure: false,
})
export class FilterPipe implements PipeTransform {
  /**
   *
   * arr | pipeName
   *
   */
  transform(arr: Array<UserModel>, propertyName: string) {
    console.log('In transform');
    return arr.filter((item) => item[propertyName] == 'Yes');
  }
}
