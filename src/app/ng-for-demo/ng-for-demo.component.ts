import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-for-demo',
  templateUrl: './ng-for-demo.component.html',
  styleUrls: [],
})
export class NgForDemoComponent {
  eventString: string = null;
  private eventArr: Array<string> = [];
  testArr: Array<string> = ['meeting at 10am', 'DINNER AT 10PM', 'lunch'];
  name: string = 'abhijeet santosh lokhande';
  salary: number = 100000;
  today: Date = new Date();

  // onChangeEventInput(e) {
  //   this.eventString = e.target.value;
  // }
  addEvent() {
    this.eventArr.push(this.eventString);
    this.eventString = null;
  }
  deleteEvent(index: number) {
    this.eventArr.splice(index, 1);
  }
  getEventsArray() {
    return this.eventArr;
  }
  changeDate() {
    this.today = new Date(2018, 7);
  }
}
