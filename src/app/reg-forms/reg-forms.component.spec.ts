import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegFormsComponent } from './reg-forms.component';

describe('RegFormsComponent', () => {
  let component: RegFormsComponent;
  let fixture: ComponentFixture<RegFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegFormsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
