import { Component, OnInit } from '@angular/core';
import { UserModel } from './../models/user.model';
import { ThrowStmt } from '@angular/compiler';
@Component({
  selector: 'app-reg-forms',
  templateUrl: './reg-forms.component.html',
  styleUrls: ['./reg-forms.component.css'],
})
export class RegFormsComponent {
  name: string = null;
  age: number = null;
  lFlag: boolean = null;
  userArr: Array<UserModel> = [];

  string_pass: string = 'I Love My India';
  number_pass: number = 1234;
  bool_pass: boolean = true;
  numberArr: Array<number> = [];

  objArray: Array<{}> = [
    {
      'number 1': 100,
      'number 2': 200,
      'number 3': 300,
    },
  ];
  objArray2: Array<{}> = [];
  onClickAdd() {
    const userObj = new UserModel(this.name, this.age, this.lFlag);
    this.userArr.push(userObj);
    this.name = null;
    this.age = null;
    this.lFlag = null;
  }
  onChangeInputParam() {
    this.string_pass = 'A--- ' + Math.ceil(Math.random() * 100) / 100;
    this.number_pass = Math.ceil(Math.random() * 100) / 100;
    this.bool_pass = !this.bool_pass;
    this.numberArr = new Array(5).fill(this.number_pass);

    const obj = {
      'number 1': this.number_pass * 2,
      'number 2': this.number_pass * 3,
      'number 3': this.number_pass * 4,
    };
    this.objArray.push(obj);

    this.objArray2.push({
      'First Name': 'A' + this.number_pass,
      'Last Name': 'B' + this.number_pass,
    });

    // const temp = this.getDeepCopy(this.objArray2);
    // this.objArray2 = null;
    // this.objArray2 = this.getDeepCopy(temp);
  }
  getDeepCopy(item) {
    return JSON.parse(JSON.stringify(item));
  }
}
