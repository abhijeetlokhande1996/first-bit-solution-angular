import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'upperCasePipe',
})
export class MyUpperCasePipe implements PipeTransform {
  transform(value: string) {
    return value.toUpperCase();
  }
}
